import numpy as np
import matplotlib.pyplot as plt
import time as tm
import sys
sys.path.insert(0, '..')
from microsig import take_image


tot_toc = 0


mic = {'magnification': 10,
       'numerical_aperture': 0.3,
       'focal_length': 350,
       'pixel_size': 6.45,
       'pixel_dim_x': 512,
       'pixel_dim_y': 64,
       'background_mean': 335,
       'background_noise': 10,
       'gain': 3.2,
       'ri_medium': 1,
       'ri_lens': 1.5,
       'points_per_pixel': 18,
       'n_rays': 500,
       'cyl_focal_length': 0,
       'z_min': -75,
       'z_depth': 85,
       'd_p': 2}

#%% test1: x, y, z, dp - dp uniform

n_particles = 12
# define particle coordinates and size
x = np.linspace(40,472,n_particles)
y = np.zeros(n_particles)+32
z = np.linspace(-60,20,n_particles)
dp = np.zeros(n_particles)+2
P = np.vstack((x,y,z,dp)).transpose()

tic = tm.time()
im1 = take_image(mic,P)
dum_toc = tm.time()-tic
print('test1: {0} sec'.format(dum_toc))
tot_toc = tot_toc+dum_toc

#%% test2: x, y, z, dp - dp non uniform

n_particles = 12
# define particle coordinates and size
x = np.linspace(40,472,n_particles)
y = np.zeros(n_particles)+32
z = np.zeros(n_particles)-20
dp = np.linspace(1,4,n_particles)
P = np.vstack((x,y,z,dp)).transpose()

tic = tm.time()
im2 = take_image(mic,P)
dum_toc = tm.time()-tic
print('test2: {0} sec'.format(dum_toc))
tot_toc = tot_toc+dum_toc

#%% test3: x, y, z, dp, Ip 

n_particles = 12
# define particle coordinates and size
x = np.linspace(40,472,n_particles)
y = np.zeros(n_particles)+32
z = np.zeros(n_particles)-20
dp = np.zeros(n_particles)+2
Ip = np.linspace(0.1,1,n_particles)
P = np.vstack((x,y,z,dp,Ip)).transpose()

tic = tm.time()
im3 = take_image(mic,P)
dum_toc = tm.time()-tic
print('test3: {0} sec'.format(dum_toc))
tot_toc = tot_toc+dum_toc

#%% test4: x, y, z, dp, alpha, beta 

mic['n_rays'] = 250
n_particles = 12
# define particle coordinates and size
x = np.linspace(40,472,n_particles)
y = np.zeros(n_particles)+32
z = np.zeros(n_particles)-20
dp = np.zeros(n_particles)+2
ep = np.zeros(n_particles)+4
alpha = np.array([0, 0, 0, 0, 
                  np.pi/8, np.pi/4, np.pi*3/8, np.pi/2,
                  np.pi/2, np.pi/2, np.pi/2, np.pi/2])
beta = np.array([0, np.pi/8, np.pi/4, np.pi*3/8,
                  np.pi/2, np.pi/2, np.pi/2, np.pi/2,
                  np.pi*3/8,  np.pi/4, np.pi/8, 0])
P = np.vstack((x,y,z,dp,ep,alpha,beta)).transpose()

tic = tm.time()
im4 = take_image(mic,P)
dum_toc = tm.time()-tic
print('test4: {0} sec'.format(dum_toc))
tot_toc = tot_toc+dum_toc

#%% test5: x, y, z, dp, alpha, beta, Ip

P = np.vstack((x,y,z,dp,ep,alpha,beta,Ip)).transpose()

tic = tm.time()
im5 = take_image(mic,P)
dum_toc = tm.time()-tic
print('test5: {0} sec'.format(dum_toc))
tot_toc = tot_toc+dum_toc

#%% test5: x, y, z, dp, astigmatism

mic['n_rays'] = 500
mic['cyl_focal_length'] = 4000
x = np.linspace(40,472,n_particles)
y = np.zeros(n_particles)+32
z = np.linspace(-60,20,n_particles)
dp = np.zeros(n_particles)+2
P = np.vstack((x,y,z,dp)).transpose()

tic = tm.time()
im6 = take_image(mic,P)
dum_toc = tm.time()-tic
print('test6: {0} sec'.format(dum_toc))
tot_toc = tot_toc+dum_toc

#%%

im = np.vstack((im1, im2, im3, im4, im5, im6))
plt.figure(0)
plt.imshow(im)
print('Totale time: {0} sec'.format(tot_toc))
